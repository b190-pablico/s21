console.log("Hello World");

/*
	Miniactivity
		create an array "cities" with the following elements
			Tokyo
			Manila
			Seoul
			Jakarta
			Sim
		log it in the console
		send the output in the chat
*/

let cities = [ "Tokyo", "Manila", "Seoul", "Jakarta", "Sim" ];
console.log(cities);
//to check for the number of elements inside the array
console.log(cities.length);
//check the last index of the array
console.log(cities.length - 1);
// check the last element through the index
console.log(cities[cities.length - 1]);

// in cases of blank arrays
blankArr = [];
console.log(blankArr);//returns empty array
console.log(blankArr.length);//returns 0
console.log(blankArr[blankArr.length]);//returns undefined


console.log(cities);
// decrement operator can be used in the .lenght property to delete the last element of the array.
cities.length --;
// cities.length = cities.length - 1
console.log(cities);

let lakersLegends = [ "Kobe", "Shaq", "Magic", "Kareem", "LeBron" ];
console.log(lakersLegends);

lakersLegends.length ++;
console.log(lakersLegends);
/*
Miniactivity
	replace the last element in the updated array with Pau Gasol using index

	send the output in the google chat
*/

lakersLegends[5] = "Pau Gasol";
console.log(lakersLegends);

// use the .length property to directly add elements at the end of the array
lakersLegends[lakersLegends.length] = "Anthony Davis";
console.log(lakersLegends);

// looping through the array
let numArray = [ 5, 12, 30, 46, 40 ];

for (let index = 0; index < numArray.length; index++) {
	if (numArray[index] % 5 === 0) {
		console.log(numArray[index] + "is divisible by 5");
	}else{
		console.log( numArray[index] + " is not divisible by 5");
	}
};

for (let index=numArray.length -1; index >= 0; index--) {
    console.log(numArray[index]);
};

// multidimensional arrays
/*
	- are useful for storing complex data structures
	- a practical application of this is to help visualize/create real world objects
	- though useful in a number of cases, creating complex arrays is not always recommended
*/
let chessBoard = [
[ "a1", "b1", "c1", "d1", "e1" ],
[ "a2", "b2", "c2", "d2", "e2" ],
[ "a3", "b3", "c3", "d3", "e3" ],
[ "a4", "b4", "c4", "d4", "e4" ],
[ "a5", "b5", "c5", "d5", "e5" ]
];
console.log(chessBoard);
// accessing a multidimensional array
console.log(chessBoard[1][4]);
console.log("Pawn moves to " + chessBoard[2][3]);