console.log("Hello World");

// miniactivity

let cities = ["Tokyo", "Manila", "Seoul", "Jakarta", "Sim"];
console.log(cities);
// to check for the number of elements inside the array
console.log(cities.length);
// check the last index of the array
console.log(cities.length-1);
// check the last element through the index
console.log(cities[cities.length-1]);

// blank arrays
blankArr = [];
console.log(blankArr); //returns empty array
console.log(blankArr.length); // returns 0
console.log(blankArr[blankArr.length]); //returns undefined

console.log(cities);
// decrement operator can be used in the .length property to delete the last element of the array.
cities.length--;
console.log(cities);

let lakersLegends = ["Kobe", "Shaq", "Magic", "Kareem", "LeBron"];
console.log(lakersLegends);

lakersLegends.length ++;
console.log(lakersLegends);

lakersLegends[5] = "Pau Gasol"
console.log(lakersLegends);

// use the .length property to directly add elements at the end of the array
lakersLegends[lakersLegends.length] = "Anthony Davis"
console.log(lakersLegends);

let numArr = [5, 12, 30, 46, 40];
for (let index = 0; index < numArr.length; index++){
	if (numArr[index] % 5 === 0){
		console.log(numArr[index] + " is divisible by 5")
	}else{
		console.log(numArr[index] + " is not divisible by 5")
	}
};

for (let index = 0; index < numArr.length; index++){
	console.log(numArr[index])
}

for( let index=numArr.length -1; index>=0; index--){
		console.log(numArr[index])
}

// multidimensional arrays
let chessBoard = [
["a1", "b1", "c1", "d1", "e1"],
["a2", "b2", "c2", "d2", "e2"],
["a3", "b3", "c3", "d3", "e3"],
["a4", "b4", "c4", "d4", "e4"],
["a5", "b5", "c5", "d5", "e5"],
];
console.log(chessBoard);



console.log(chessBoard[1][4]);